#include <SoftwareSerial.h>
#include <DHT.h>
#include <SPI.h>
#include <EEPROM.h>
#include <stdlib.h>
#include <avr/sleep.h>

#define DHTPIN 12
#define DHTTYPE DHT22
#define BT_RXD 10
#define BT_TXD 11
SoftwareSerial bluetooth(BT_RXD, BT_TXD);
DHT dht(DHTPIN, DHTTYPE);

int h, t, cds, m;
int j = 0;
int flag = 0;
int i = 0;
int k = 0;

char text;
char data[17] = { NULL, };

char year[3];
char year2[3];
char month[3];
char day[3];
char hour[3];
char minute[3];

int year1_int, year2_int, month_int, day_int, hour_int, minute_int;

int year_s, month_s, day_s, hour_s, minute_s;

int data_int[4];

extern volatile unsigned long timer0_millis;

unsigned long time1 = 0;
unsigned long previousTime = 0;

void setup() {
  Serial.begin(9600);
  bluetooth.begin(9600);
  dht.begin();
  pinMode(A1, INPUT);
  pinMode(12, INPUT);
}

void loop() {
  if(bluetooth.available()) {
    text = bluetooth.read();
    if(text > 0) {
      if(text > 43 && text < 58 && data[k] == NULL) {
        data[k] = text;
        k++;
      }
      if(k == 16) {
        data[k] = '0';
        Serial.print("DATA : ");
        Serial.println(data);

        delay(100);
      
        for(int l = 0; l < 2; l++) {
          year[l] = data[l];
          year2[l] = data[l + 2];
          month[l] = data[l + 5];
          day[l] = data[l + 8];
          hour[l] = data[l + 11];
          minute[l] = data[l + 14];    
          delay(100);   
        }
        delay(100);

        year1_int = atoi(year);
        year2_int = atoi(year2);
        month_int = atoi(month);
        day_int = atoi(day);
        hour_int = atoi(hour);
        minute_int = atoi(minute);

        delay(100);
        timer0_millis = 0;
        previousTime = time1;
        k++;
      }
    }
  }

  if(digitalRead(2)){
    if(data[16] != NULL) {
      bt_send();
    }        
  }

  if(data[16] != NULL) {
    timer();
  }
}

void bt_send() {
      if(EEPROM.read(1) != 0) {
        for(int i = 1 + EEPROM.read(1023); i < 1014; i++) {
          if(EEPROM.read(i) == 0) {
            EEPROM.write(0, 0);
            Serial.println("cnt reset");
            timer0_millis = 0;
            previousTime = time1;
            break;
          }
          else {
            if(flag == 0) {
              delay(2000);
              flag = 1;
            }
         
            Serial.print("SEND EEPROM DATA SET [");
            Serial.print(i);
            Serial.println("]");
        
            char send_date[22];
            sprintf(send_date, "%03d%02d%02d%02d%02d", EEPROM.read(1018), EEPROM.read(1019),EEPROM.read(1020),EEPROM.read(1021),EEPROM.read(1022));
        
            bluetooth.print(send_date);
            bluetooth.print("/");

            char send_sensor[12];
            sprintf(send_sensor, "%02d%02d%02d%02d", EEPROM.read(i),EEPROM.read(i+1),EEPROM.read(i+2),EEPROM.read(i+3));
        
            bluetooth.println(send_sensor);
            EEPROM.write(i, 0);
            EEPROM.write(i+1, 0);
            EEPROM.write(i+2, 0);
            EEPROM.write(i+3, 0);
        
            EEPROM.write(1022, EEPROM.read(1022) + 2);
            if(EEPROM.read(1022) > 24) {
              EEPROM.write(1021, EEPROM.read(1021) + 1);
              EEPROM.write(1022, 0);
              if(EEPROM.read(1021) > 31) { 
                EEPROM.write(1020, EEPROM.read(1020) + 1);
                EEPROM.write(1021, 1);
                sprintf(send_sensor, "%02d%02d%02d%02d", EEPROM.read(i),EEPROM.read(i+1),EEPROM.read(i+2),EEPROM.read(i+3));
                bluetooth.println(send_sensor);
              }
            }            

            i += 3;
            delay(20);

            if(EEPROM.read(1023) != 0 && i > 1016) {
              i = 1;
            }
          }
          delay(100);
        }
      }

      else {
        flag = 0;
    
        readSensor();
  
        char data[13];
        sprintf(data, "%03d,%02d,%02d,%02d", t, h, (100 - cds), (100 - m));
        bluetooth.println(data);
  
        delay(1000);
      }
}

void timer() {
  time1 = millis();

  if(time1 - previousTime >= 1000) {
    Serial.print(time1 / 1000);
    Serial.print(",");
    previousTime = time1;
  }

  if(time1 - previousTime >= 60000) {
    minute_int += 1;
    previousTime = time1;
  }

  //if(minute_int > 59) {
  if(time1 >= 30000) {
    save_data();
  }
}

void save_data() {
    hour_int += 1;

    if(hour_int > 24) {
      hour_int = 0;
      day_int += 1;
    }

    if(hour_int % 2 == 0) {
      if(EEPROM.read(0) == 0) {
        EEPROM.write(1018, year1_int);
        EEPROM.write(1019, year2_int);
        EEPROM.write(1020, month_int);
        EEPROM.write(1021, day_int);
        EEPROM.write(1022, hour_int);
      }
      
      readSensor();

      data_int[0] = t;
      data_int[1] = h;
      data_int[2] = (100- cds);
      data_int[3] = (100 - m);
    
      char data[13];
      sprintf(data, "%03d,%02d,%02d,%02d", t, h, (100 - cds), (100 - m));
      Serial.println("");
      Serial.println(data);
    
      for(int i = 1 + EEPROM.read(0) * 4; i < 5 + EEPROM.read(0) * 4; i++) {
        EEPROM.write(i, data_int[j]);
        Serial.print("SAVING EEPROM DATA [");
        Serial.print(i);
        Serial.print("] : ");
        Serial.println(EEPROM.read(i));
        j++;
        delay(100);
      }
      Serial.print("FIRST SAVING TIME : ");
      for(int i = 1019; i < 1023; i++)
        Serial.print(EEPROM.read(i));
      Serial.println("");
    
      j = 0;
      EEPROM.write(0, EEPROM.read(0) + 1);
      if(EEPROM.read(0) > 253) {
        EEPROM.write(0, 0);
        EEPROM.write(1023, EEPROM.read(1023) + 4);
      }

      if(EEPROM.read(1023) != 0) {
        EEPROM.write(1023, EEPROM.read(1023) + 4);
        EEPROM.write(1022, EEPROM.read(1022) + 2);
      }
      Serial.print("Current EEPROM[0] : ");
      Serial.println(EEPROM.read(0) * 4);
      minute_int = 0;
      timer0_millis = 0;
      previousTime = time1;
    }
  //}
}

void readSensor() {
  h = map(dht.readHumidity(), 0, 100, 1, 100);
  t = dht.readTemperature();
  cds = map(analogRead(A1), 0, 1023, 1, 100);
  m = map(analogRead(A2), 0, 1023, 1, 100);
  
  if(h == 0 || h > 100) h = 1;
  else if(t == 0) t = 1; 
}
